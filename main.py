from func import create_gene_list, convert_gene_to_uniprot_id, is_my_gene_an_enzyme, get_url, create_dic_trad, is_kegg_exist, convert_to_kegg_enzyme_id, get_reactions
import requests
import re
# DBLINKS in my_request = "https://rest.kegg.jp/get/" + item for all id converted names
"""
# create gene list from csv file given
gene_list = create_gene_list("short.csv")

#uniprot_ids = ['P04233', 'P10909', 'P68104', 'P13639', 'P01920', 'P01911', 'P40305', 'Q53G44', 'P09912', 'Q01628', 'Q9GZW8', 'P00846', 'P00395', 'P00403', 'P00156', 'P03905', 'P55209', 'P11940', 'P11309', 'Q9NZF1', 'Q6P4A8', 'P39023', 'P15880', 'P62701', 'P80511', 'P05109', 'P06702', 'P14151', 'Q9H3M7', 'P13611']
uniprot_ids = []
# convert genes names to uniprot ids
for item in gene_list:
    uniprot_id = convert_gene_to_uniprot_id(item)
    if(uniprot_id is not None):
        uniprot_ids.append(uniprot_id)

#enzyme_list = ['P00395', 'P00403', 'P03905', 'P11309', 'Q6P4A8', 'P10153', 'P00414', 'P12236', 'P03886', 'P14618', 'P23141', 'P07333', 'Q9Y6K5', 'P29728', 'P11169', 'P33121', 'P17844', 'P10619', 'P16671', 'P03897', 'P07339', 'P03923', 'P03891', 'P28562', 'P23396', 'Q8WXG1', 'P19971', 'P06733', 'Q8IXQ6', 'Q16678', 'P25774', 'P07858', 'P03915', 'P55265', 'O15162', 'P28838', 'Q12974', 'P21673', 'P04406', 'P52209', 'P09211', 'P00488', 'P00338', 'Q5H9U9', 'P15144', 'P00746', 'Q63HN8', 'P32455', 'P15104', 'P07948', 'P08575']
enzyme_list = []
# check if genes are enzymes
for item in uniprot_ids:
    if(is_my_gene_an_enzyme(item)):
        enzyme_list.append(item)

# find conversion uniprot/kegg tab in kegg API and save it
dic_trad = create_dic_trad()

# kegg_ids = ["hsa:5243", "hsa:2180", "hsa:23305", "hsa:56895", "hsa:26289"]
kegg_ids = []
# verify if uniprot entry exist in KEGG
for item in enzyme_list:
    if is_kegg_exist(dic_trad,item):
        kegg_ids.append(dic_trad[item])

#(code à modifier pour lire tous les gènes diff dans kegg et pas seulement ceux associés à des enzymes sur uniprot)
enzyme_ids = []
# check for enzyme associated id in KEGG, if exists
for item in kegg_ids:
    enzyme_id = convert_to_kegg_enzyme_id(item)
    if enzyme_id is not None:
        enzyme_ids.append(enzyme_id)

print(enzyme_ids)
"""

enzyme_ids = ['ec:7.1.1.9', 'ec:7.1.1.2', 'ec:2.7.11.1']

for item in enzyme_ids:
    get_reactions(item)

# next setp : find which molecules in the raction are metabolites and which are side compounds"""