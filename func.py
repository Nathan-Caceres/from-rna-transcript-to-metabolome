import requests
import sys
import csv
import re

def convert_to_kegg_enzyme_id(p_item):
    try:
        # try to get KEGG enzyme webpage
        my_request = "https://rest.kegg.jp/get/" + p_item
        r = requests.get(my_request)
        # check if enzyme name exist on the page
        enzyme_id = re.search("\[EC:.*\]",r.text)
        # if yes, remove unnecessary characters
        if enzyme_id is not None:
            enzyme_id = enzyme_id[0]
            enzyme_id = enzyme_id.replace("[","")
            enzyme_id = enzyme_id.replace("]", "")
            enzyme_id = enzyme_id.replace("EC", "ec")
            print(enzyme_id, " found for gene ", p_item)
            # and return enzyme id
            return enzyme_id
        else:
            print("No associated enzyme found in kegg for ", p_item)
            return None
    except:
        print("enzyme id association error")

def is_kegg_exist(p_dict,p_item):
    try:
        # check if item exist in the conversion dict
        if p_item in p_dict:
            print(p_dict[p_item], " exist in kegg")
            return True
        else:
            print(p_item, " not found on kegg")
            return False
    except:
        print("error fiding gene associated with ", p_item)

def create_dic_trad():
    try:
        # download conversion tab from KEGG
        my_request = 'http://rest.kegg.jp/conv/hsa/uniprot'
        r = requests.get(my_request)
        # write conversion tab to a txt file
        with open("tab_uniprot_to_kegg.txt", "w") as f:
            f.write(r.text)
        dic_trad = {}
        f.close()
        # open txt conversion file for reading
        with open("tab_uniprot_to_kegg.txt", "r") as f:
            lines = f.readlines()
            for line in lines:
                line = line.split()
                # remove unnecessary words
                line[0] = line[0].replace("up:", "")
                # fill dictionnary
                dic_trad[line[0]] = line[1]
        f.close()
        # return dictionnary
        return dic_trad
    except:
        print("probelm in dictionnary creation")
        return 0

# API adress
API_URL = "https://rest.uniprot.org"

# fonction to determine if a gene is stored as an enzyme on uniprot
def is_my_gene_an_enzyme(gene):
    # request
    r = get_url(f"{API_URL}/uniprotkb/{gene}")
    # looking for term "CATALYTIC ACTIVTY" in gene resume
    if "CATALYTIC ACTIVITY" in r.text:
        print(gene, " is an enzyme")
        return True
    else:
        print(gene, " is not an enzyme")
        return False

def create_gene_list(file):
    try:
        with open(file) as f:
            gene_list = []
            reader = csv.DictReader(f)
            for row in reader:
                # colname for gene is the only that have empty name
                gene_list.append(row[''])
        print("gene list created")
        return gene_list
    except:
        print("Gene list creation error")
        return 0

def convert_gene_to_uniprot_id(gene_name):
    # uniprot query
    r = get_url(f"{API_URL}/uniprotkb/stream?query={gene_name} AND (taxonomy_id:9606)")
    data = r.json()
    n_results = len(data["results"])
    # parsing uniprot results
    for i in range(n_results):
        # verify gene category exist
        try:
            if "genes" in data["results"][i].__str__():
                # check for exact gene name and if it's reviewed
                if data["results"][i]["genes"][0]["geneName"]["value"].__str__() == gene_name and "UniProtKB reviewed" in data["results"][i]["entryType"]:
                    print(gene_name, " = ", data["results"][i]["primaryAccession"])
                    # return uniprot id
                    return data["results"][i]["primaryAccession"]
        except:
            print(gene_name," conversion failed")
            return None

# function taken on the EMBL website to lunch url request
def get_url(url, **kwargs):
    response = requests.get(url,**kwargs)
    if not response.ok:
        print(response.text)
        response.raise_for_status()
        sys.exit()
    return response

enzyme_ids = ['ec:7.1.1.9', 'ec:7.1.1.2', 'ec:2.7.11.1']

def get_reactions(p_enzyme_id):
    # get reactions associated to enzyme p_enzyme_id
    my_request = "https://rest.kegg.jp/link/reaction/" + p_enzyme_id
    r = requests.get(my_request)
    result = r.text
    result = result.split()
    # keep only reaction id
    result = result[1]
    # get reaction webpage
    my_request_2 = "https://rest.kegg.jp/get/" + result
    r2 = requests.get(my_request_2)
    # display reactions infos
    print(r2.text)